#include "Credentials.h"

Credentials::Credentials(QString userId, QString accessToken)
{
	this->userId = userId;
	this->accessToken = accessToken;
}

Credentials::Credentials()
{
	ReadFromFile();
}

void Credentials::SaveToFile()
{
    QFile file("data.bin");
	file.open(QIODevice::WriteOnly);
	QTextStream stream(&file);
	stream << this->userId + "\n" << this->accessToken;
	file.close();
}

void Credentials::DeleteFile()
{
    QFile file("data.bin");
    file.open(QIODevice::WriteOnly);
    file.close();
}

bool Credentials::IsValid()
{
	RequestManager::Init(userId, accessToken);
	auto response = RequestManager::GetUserInfo();
    if (response.contains("error") || response.empty()) return false;
	return true;
}

void Credentials::ReadFromFile()
{
    QFile file("data.bin");
	file.open(QIODevice::ReadOnly);
	if (file.size() == 0)
	{
		file.close();
		return;
	}
	QTextStream stream(&file);
	userId = stream.readLine();
	accessToken = stream.readLine();
	file.close();
}

QString Credentials::GetUserId()
{
	return this->userId;
}
QString Credentials::GetAccessToken()
{
	return this->accessToken;
}
