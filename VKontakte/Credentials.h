#pragma once
#include <QString>
#include <QNetworkReply>
#include "RequestManager.h"
#include <QFile>
#include <QTextStream>

using namespace  std;

class Credentials
{
	QString userId;
	QString accessToken;
	
	void ReadFromFile();

public:
	Credentials();
	Credentials(QString userId, QString accessToken);
	void SaveToFile();
	bool IsValid();
    void DeleteFile();
	QString GetUserId();
	QString GetAccessToken();
};

