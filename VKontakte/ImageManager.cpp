#include "ImageManager.h"

QString ImageManager::CreateDirectory(QString dirName)
{
    if (!QDir("images").exists())
        QDir(QDir().absolutePath()).mkdir("images");
    QDir("images").mkdir(dirName);
    return "images/"+dirName + "/";
}

bool ImageManager::IsExist(QString dirName,QString imageName)
{
    QDir directory(dirName);
    return directory.exists(imageName);
}

QString ImageManager::ProcessImage(QString url, int userId= 0)
{
    QString dirName = QString::number(userId);
    dirName = CreateDirectory(dirName);
    auto imageName = url.split('/').back();
    if (!IsExist(dirName,imageName)) DownloadImage(url, dirName+imageName);
    return dirName + imageName;
}

void ImageManager::SaveImage(QString imageName, QNetworkReply *reply)
{
    QPixmap img;
    QFile file(imageName);
    file.open(QIODevice::WriteOnly);
    img.loadFromData(reply->readAll());
    img.save(&file);
    file.close();
}

void ImageManager::DownloadImage(QString url, QString imageName)
{
    //qDebug() << "Download image from " + url;
    QEventLoop eventLoop;
    auto manager = new QNetworkAccessManager();
    QNetworkRequest request(url);
    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
    auto reply = manager->get(request);
    eventLoop.exec();
    SaveImage(imageName, reply);
}
