#pragma once
#include <QString>
#include <QDir>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QNetworkReply>
#include <QPixmap>
#include <QBitmap>
#include <QLabel>
#include <QFile>

class ImageManager
{
    static bool IsExist(QString dirName,QString imageName);
    static QString CreateDirectory(QString dirName);
    static void DownloadImage(QString url, QString imageName);
    static void SaveImage(QString imageName, QNetworkReply *reply);
public:
    static QString ProcessImage(QString url, int userId);
};
