#include "RequestManager.h"
#include "MainWindow.h"

QString RequestManager::_currentUserId = "";
QString RequestManager::_accessToken = "";
QString RequestManager::_longPollKey = "";
QString RequestManager::_longPollServer = "";
QMap<QString, QJsonObject> RequestManager::_cache;
int RequestManager::_longPollTS = 0;

void RequestManager::Init(QString userId, QString accessToken)
{
	_accessToken = accessToken;
	_currentUserId = userId;
    auto jsonString = SendRequest("messages.getLongPollServer","use_ssl=1");
    auto json = StringToQJsonObject(jsonString).value("response").toObject();
    _longPollKey = json.value("key").toString();
    _longPollServer = json.value("server").toString();
    _longPollTS = json.value("ts").toInt();
}

QString RequestManager::GetCurrentUser()
{
    return _currentUserId;
}

QJsonArray RequestManager::LongPollServerRequest()
{
    QEventLoop eventLoop;
    QNetworkAccessManager netManager;
    QString startUrl = "https://" + _longPollServer + "?act=a_check&wait=25&mode=2&key=" +
            _longPollKey+"&ts=" + QString::number(_longPollTS);
    //qDebug() << "_____Request to: " + startUrl;
    auto url = QUrl(startUrl);
    QNetworkRequest request(url);
    auto response = netManager.get(request);
    QObject::connect(&netManager, SIGNAL(finished(QNetworkReply*)),&eventLoop,SLOT(quit()));
    eventLoop.exec();
    auto jsonString = (QString) response->readAll();
    auto json = StringToQJsonObject(jsonString);
    _longPollTS = json.value("ts").toInt();
    return json.value("updates").toArray();
}

//API methods

QJsonObject RequestManager::Like(QString params)
{
    return StringToQJsonObject(SendRequest("likes.add", params)).value("response").toObject();
}

QJsonObject RequestManager::Dislike(QString params)
{
    return StringToQJsonObject(SendRequest("likes.delete", params)).value("response").toObject();
}

QJsonObject RequestManager::LikePost(int ownerId, int itemId, QString accessKey)
{
    auto params = "type=post&owner_id=" + QString::number(ownerId) +
                  "&item_id=" + QString::number(itemId) +
                  (accessKey.isEmpty() ? accessKey: "&access_key" + accessKey);
    return Like(params);
}

QJsonObject RequestManager::DislikePost(int ownerId, int itemId, QString accessKey)
{
    auto params = "type=post&owner_id=" + QString::number(ownerId) +
                  "&item_id=" + QString::number(itemId) +
                  (accessKey.isEmpty() ? accessKey: "&access_key" + accessKey);
    return Dislike(params);
}

QJsonObject RequestManager::GetNewsFeed(QString count,QString offset)
{
    auto data = SendRequest("newsfeed.get","filters=post&count="+count+"&offset="+offset);
    return StringToQJsonObject(data).value("response").toObject();
}

void RequestManager::GetNewsFeedAsync(MainWindow *window, QString offset, QString count)
{
    auto netManager = SendRequestAsync("newsfeed.get","filters=post&count="+count+"&offset="+offset);
    QObject::connect(netManager, &QNetworkAccessManager::finished,[window](QNetworkReply* reply){
        auto replyString = (QString)reply->readAll();
        window->AddNews(StringToQJsonObject(replyString).value("response").toObject());
        window->SetNewsUpdated();
    });
}

QJsonArray RequestManager::GetFriends(QString count, QString offset)
{
    auto data = SendRequest("friends.get","order=hints&count="+count+"&offset="+offset+
                            "&fields=uid,photo_medium,contacts");
    return StringToQJsonObject(data).value("response").toArray();
}

void RequestManager::GetFriendsAsync(MainWindow *window, QString offset, QString count)
{
    auto netManager = SendRequestAsync("friends.get","order=hints&count="+count+"&offset="+offset+
                                       "&fields=uid,photo_medium,contacts,home_town");
    QObject::connect(netManager, &QNetworkAccessManager::finished,[window](QNetworkReply* reply){
        auto replyString = (QString)reply->readAll();
        window->AddFriends(StringToQJsonObject(replyString).value("response").toArray());
        window->SetFriendsUpdated();
    });
}

QJsonObject RequestManager::GetUserInfo(int userId)
{
    return GetUserInfo(QString::number(userId));
}

QJsonObject RequestManager::GetUserInfo(QString userId)
{
    if (_cache.contains(userId)) return _cache.value(userId);
    auto jsonString = SendRequest("users.get", "user_ids=" + userId +
                            "&fields=screen_name,photo_200_orig,online,photo_50,home_town,last_seen,contacts,status,connections");
    jsonString.replace("[","").replace("]","");
    auto result = StringToQJsonObject(jsonString).value("response").toObject();
    if (!result.empty() && !result.contains("error")) _cache.insert(userId,result);
    return result;
}

QJsonArray RequestManager::GetProfilePhotos(int userId, int offset, int count)
{
    auto data = SendRequest("photos.getProfile","owner_id=" + QString::number(userId)+"&count=" +
                            QString::number(count) + "&offset=" + QString::number(offset));
    return StringToQJsonObject(data).value("response").toArray();
}

void RequestManager::SetOnline()
{
    SendRequestAsync("account.setOnline","");
}

void RequestManager::SetOffline()
{
    SendRequestAsync("account.setOffline","");
}

//MESSAGES

void RequestManager::SendMessage(QString recepient, QString message)
{
    SendRequestAsync("messages.send",recepient + message);
}

void RequestManager::MarkMessagesAsRead(int messageId)
{
    QString ids = "message_ids="+ QString::number(messageId);
    SendRequestAsync("messages.markAsRead",ids);
}

QJsonObject RequestManager::GetMessageById(QString mid)
{
    auto jsonString = SendRequest("messages.getById","message_ids=" + mid);
    auto jsonArray = StringToQJsonArray(jsonString);
    return jsonArray.first().toObject();
}

QJsonArray RequestManager::GetMessageHistory(QString userId, QString offset, bool reversed)
{
    auto jsonString = SendRequest("messages.getHistory","user_id=" + userId + "&offset=" + offset + "&count=10");
    auto jsonArray = StringToQJsonArray(jsonString);
    return reversed ? ReverseArray(jsonArray) : jsonArray;
}

QJsonArray RequestManager::GetChatMessageHistory(QString chatId, QString offset,bool reversed)
{
    auto jsonString = SendRequest("messages.getHistory","chat_id=" + chatId + "&offset=" + offset + "&count=10");
    auto jsonArray = StringToQJsonArray(jsonString);
    return reversed ? ReverseArray(jsonArray) : jsonArray;
}

QJsonObject RequestManager::GetUserMessages()
{
    auto jsonString = SendRequest("messages.get","count=1");
    return StringToQJsonObject(jsonString);
}

QJsonArray RequestManager::GetDialogs(QString offset, QString count)
{
    auto jsonString = SendRequest("messages.getDialogs","preview_length=20&offset=" + offset+ "&count="+count);
    auto array = StringToQJsonArray(jsonString);
    return array;
}

void RequestManager::GetDialogsAsync(MainWindow *window, QString offset, QString count)
{
    auto netManager = SendRequestAsync("messages.getDialogs","preview_length=20&offset=" + offset+ "&count="+count);
    QObject::connect(netManager, &QNetworkAccessManager::finished,[window](QNetworkReply* reply){
        auto replyString = (QString)reply->readAll();
        auto array = StringToQJsonArray(replyString);
        ThreadManager::GetInstance()->Cache(array);
        window->AddConversations(array);
        window->SetConversationsUpdated();
    });
}

QJsonObject RequestManager::GetMessagesUploadServerForPhotos()
{
    auto jsonString = SendRequest("photos.getMessagesUploadServer");
    return ResponseToObject(jsonString);
}

QJsonObject RequestManager::GetDocsUploadServer()
{
    auto jsonString = SendRequest("docs.getUploadServer");
    return ResponseToObject(jsonString);
}

QJsonArray RequestManager::SaveMessagesPhoto(QJsonObject json)
{
    auto jsonString = SendRequest("photos.saveMessagesPhoto", "server=" + QString::number(json.value("server").toInt()) +
                                  "&hash=" + json.value("hash").toString() + "&photo=" + json.value("photo").toString());
    return StringToQJsonObject(jsonString).value("response").toArray();
}

QJsonArray RequestManager::SaveDoc(QString serverUrl,QString filePath)
{
    auto result = StringToQJsonObject(SendPostToServer(serverUrl, filePath, "text/plain"));  //save result of this method
    auto fileString = SendRequest("docs.save","file=" + result.value("file").toString() +
                                  "&title=" + filePath.split("/").last());
    return StringToQJsonArray(fileString,false);
}

QJsonObject RequestManager::PostImageToServer(QString serverUrl, QString imagePath)
{
    return StringToQJsonObject(SendPostToServer(serverUrl,imagePath,"image/jpeg"));
}

//SUPPORTS

QString RequestManager::SendPostToServer(QString serverUrl, QString filePath, QString contentType)
{
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly)) return "error";
    auto eventLoop = new QEventLoop();
    auto networkManager = new QNetworkAccessManager();
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply*)),eventLoop,SLOT(quit()));
    auto url = QUrl(serverUrl);
    QNetworkRequest request(url);

    QByteArray data;
    data.append("--XXX\r\n");  //boundary
    data.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + filePath.split("/").last() + "\"\r\n");
    data.append("Content-Type: " + contentType + "\r\n\r\n");    //\r\n x 2 !

    data.append(file.readAll());
    data.append("\r\n--XXX--\r\n");
    request.setRawHeader("Content-Type","multipart/form-data; boundary=XXX");
    request.setRawHeader("Content-Length", QString::number(data.length()).toUtf8());
    auto reply = networkManager->post(request,data);
    eventLoop->exec();
    auto str = (QString)reply->readAll();
    delete reply;
    delete networkManager;
    return str;
}

QNetworkAccessManager *RequestManager::SendRequestAsync(QString method, QString params)
{
    auto networkManager = new QNetworkAccessManager();
    QString startUrl = "https://api.vk.com/method/";
    startUrl = startUrl.append(method).append("?").append(params).append("&access_token=").append(_accessToken);
    //qDebug() << "_____Request to: " + startUrl;
    auto url = QUrl(startUrl);
    QNetworkRequest request(url);
    networkManager->get(request);
    return networkManager;
}

QString RequestManager::SendRequest(QString method, QString params)
{
    QEventLoop eventLoop;
    auto networkManager = new QNetworkAccessManager();
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply*)),&eventLoop,SLOT(quit()));
	QString startUrl = "https://api.vk.com/method/";
	startUrl = startUrl.append(method).append("?").append(params).append("&access_token=").append(_accessToken);
    //qDebug() << "_____Request to: " + startUrl;
	auto url = QUrl(startUrl);
    request:
        QNetworkRequest request(url);
        auto response = networkManager->get(request);
        eventLoop.exec();
        auto returnString = (QString)response->readAll();

        //qDebug() << "_____Response:" << returnString;
        if (returnString.contains("\"error_code\":6")) goto request;    //too many requests
    if (returnString.contains("\"error_msg\":")) ParseError(returnString, method);
    delete response;
    delete networkManager;
    return returnString;
}

QJsonObject RequestManager::StringToQJsonObject(QString json)
{
    auto jsonObject = QJsonDocument::fromJson(json.toUtf8()).object();
    //qDebug() <<"______Json object: "  << jsonObject;
    return jsonObject;
}

QJsonObject RequestManager::ResponseToObject(QString json)
{
    return StringToQJsonObject(json).value("response").toObject();
}

QJsonArray RequestManager::StringToQJsonArray(QString jsonString, bool removeFirst)
{
    auto json = StringToQJsonObject(jsonString);
    auto array = json.value("response").toArray();
    if (removeFirst) array.removeFirst();    //for some requests. they have some first element
    return array;
}

QJsonArray RequestManager::ReverseArray(QJsonArray array)
{
    QJsonArray reversedArray;
    foreach (auto jsonItem, array) {
        reversedArray.push_front(jsonItem);
    }
    return reversedArray;
}

void RequestManager::RefreshCacheValue(int id)
{
    _cache.remove(QString::number(id));
    GetUserInfo(QString::number(id));
}

void RequestManager::ParseError(QString errorJson, QString method)
{
    auto json = StringToQJsonObject(errorJson);
    auto message = json.value("error").toObject().value("error_msg").toString();
    //TODO: show error message on MainWindow

}
