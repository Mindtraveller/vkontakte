#ifndef REQUESTMANAGER_H
#define REQUESTMANAGER_H
#include <QString>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonArray>
#include <QMap>
#include <QByteArray>
#include <QtCore/qeventloop.h>

//example of api method: https://api.vk.com/method/METHOD_NAME?PARAM&access_token=

enum AsyncOperations{
    Conversations, MessageHistory, News, Other
};

class MainWindow;

class RequestManager
{
    static QMap<QString, QJsonObject> _cache;  //cache users info
	static QString _accessToken;
    static QString _longPollKey;
    static QString _longPollServer;
    static int _longPollTS;
	static QString _currentUserId;

    static QString SendRequest(QString method, QString params = "");
    static QNetworkAccessManager* SendRequestAsync(QString method, QString params);
    static QString SendPostToServer(QString serverUrl, QString filePath, QString contentType);

    static QJsonArray ReverseArray(QJsonArray array);
    static QJsonObject Like(QString params);
    static QJsonObject Dislike(QString params);

    static QJsonObject StringToQJsonObject(QString data);
    static QJsonObject ResponseToObject(QString json);
    static QJsonArray StringToQJsonArray(QString jsonString, bool removeFirst = true);


    static void ParseError(QString errorJson, QString method);
public:
	static void Init(QString userId, QString accessToken);
    static QJsonObject GetUserInfo(int userId);
    static QJsonObject GetUserInfo(QString userId = _currentUserId);
    static QJsonArray GetProfilePhotos(int userId, int offset = 0, int count = 10);

    static QJsonArray GetFriends(QString count="20", QString offset = "0");
    static void GetFriendsAsync(MainWindow *window, QString offset = "0", QString count="20");
    static QJsonObject GetNewsFeed(QString count = "10", QString offset = "0");
    static void GetNewsFeedAsync(MainWindow *window, QString offset = "0", QString count = "10");
    static QString GetCurrentUser();
    static QJsonArray LongPollServerRequest();
    static QJsonObject DislikePost(int ownerId, int itemId, QString accessKey = "");
    static QJsonObject LikePost(int ownerId, int itemId, QString accessKey = "");

    static void SetOnline();
    static void SetOffline();

    //messages
    static void MarkMessagesAsRead(int messageId);
    static void SendMessage(QString recepient, QString message);
    static QJsonObject GetMessageById(QString mid);
    static QJsonArray GetMessageHistory(QString userId,QString offset = "0",bool reversed = true);
    static QJsonArray GetChatMessageHistory(QString chatId, QString offset = "0", bool reversed = true);
    static QJsonArray GetDialogs(QString offset = "0", QString count = "15");
    static void GetDialogsAsync(MainWindow *window, QString offset ="0", QString count = "15");
    static QJsonObject GetUserMessages();

    static QJsonObject GetMessagesUploadServerForPhotos();
    static QJsonObject GetDocsUploadServer();

    static QJsonObject PostImageToServer(QString serverUrl, QString imagePath);
    static QJsonArray SaveDoc(QString serverUrl, QString filePath);

    static QJsonArray SaveMessagesPhoto(QJsonObject json);

    static void RefreshCacheValue(int id);
};
#endif

