#include "StartWindow.h"

void StartWindow::Show()
{
	auto *view = new QWebView();
	view->setUrl(authUrl);
	view->show();
	QObject::connect(view, &QWebView::urlChanged, [&, view]() {
		if (ParseUrl(view->url().toString()))
		{
			view->close();
		}
		qDebug() << "Url being viewed:" << view->url().toString();
	});
}

bool StartWindow::ParseUrl(QString url)
{
	userId = url.section("user_id=", 1, 1);
	accessToken = url.section(QRegularExpression("(access_token=|&)"), 1, 1);
    return url.contains("user_id");
}

QString StartWindow::GetUserId()
{
	return userId;
}

QString StartWindow::GetAccessToken()
{
	return accessToken;
}
