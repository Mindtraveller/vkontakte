#include <QtCore/QCoreApplication>
#include <QtGui/QtGui>
#include <QWebView>

class StartWindow
{
    QUrl authUrl = QUrl("https://oauth.vk.com/authorize?client_id=4784842&scope=messages,wall,offline,friends,photos,docs&"
                        "display=wap&v=5.30&response_type=token");
	QString userId;
	QString accessToken;
public:
	void Show();
	QString GetUserId();
	QString GetAccessToken();
private:
	bool ParseUrl(QString url);
};
