TEMPLATE = app
TARGET = VKontakte
DESTDIR = ../Win32/Debug
QT += core network webkit widgets gui webkitwidgets
CONFIG += debug console
DEFINES += WIN64 QT_DLL QT_NETWORK_LIB QT_WEBKITWIDGETS_LIB QT_WIDGETS_LIB
INCLUDEPATH += . \
    ./GeneratedFiles/Debug
DEPENDPATH += .
MOC_DIR += ./GeneratedFiles/debug
OBJECTS_DIR += debug
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles
HEADERS += ./Credentials.h \
    ./RequestManager.h \
    ./StartWindow.h	\
	./MainWindow.h \
    ./ImageManager.h \
    ./ConversationsListItem.h \
    textedit.h \
    threadmanager.h \
    basethread.h \
    longpollthread.h \
    cachethread.h \
    listview.h \
    sleepthread.h \
    setonlinethread.h
SOURCES += ./Credentials.cpp \
    ./main.cpp \
    ./RequestManager.cpp \
    ./StartWindow.cpp \
	./MainWindow.cpp \
    ./ImageManager.cpp \
    textedit.cpp \
    threadmanager.cpp \
    basethread.cpp \
    longpollthread.cpp \
    cachethread.cpp \
    listview.cpp \
    sleepthread.cpp \
    setonlinethread.cpp

FORMS    += MainWindow.ui
