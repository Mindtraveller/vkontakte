#include "basethread.h"

BaseThread::BaseThread()
{
    this->isRun = true;
}

void BaseThread::Close()
{
    this->isRun = false;
}

BaseThread::~BaseThread()
{

}

