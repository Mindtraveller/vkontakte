#ifndef BASETHREAD_H
#define BASETHREAD_H

#include <QRunnable>
#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QThread>
#include <QString>
#include "RequestManager.h"

class BaseThread: public QObject,public QRunnable
{
    Q_OBJECT
protected:
    bool isRun;
public:
    BaseThread();
    void run() = 0;
    void Close();
    ~BaseThread();
signals:
    void Done();
    void Update(QJsonArray);
    void Update(QJsonArray, int);
    void Update(QJsonObject, int);
};

#endif // BASETHREAD_H
