#include "cachethread.h"

CacheThread::CacheThread()
{

}

CacheThread::CacheThread(QList<QString> list)
{
    SetListForCache(list);
}

void CacheThread::run()
{
    //qDebug() << "_____" << this->chacheList;
    foreach (auto userId, this->chacheList) {
        RequestManager::GetUserInfo(userId);
    }
    //emit Done();
}

void CacheThread::SetListForCache(QList<QString> list)
{
    this->chacheList = list;
}

CacheThread::~CacheThread()
{

}

