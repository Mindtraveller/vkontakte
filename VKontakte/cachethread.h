#ifndef CACHETHREAD_H
#define CACHETHREAD_H
#include "basethread.h"
#include <QList>

class CacheThread : public BaseThread
{
    QList<QString> chacheList;
public:
    CacheThread();
    CacheThread(QList<QString> list);
    void run();
    void SetListForCache(QList<QString> list);
    ~CacheThread();
};

#endif // CACHETHREAD_H
