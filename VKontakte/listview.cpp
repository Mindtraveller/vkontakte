#include "listview.h"

ListView::ListView(QWidget *frame):QListWidget(frame)
{
    this->isUpdated = true;
    this->direction = true;
}

ListView::~ListView()
{
}

void ListView::SetUpdated()
{
    this->isUpdated = true;
}

void ListView::SetDirection(bool direction)  // true - down
{
    this->direction = direction;
}

bool ListView::IsNeedUpdate()
{
    auto scroll = this->verticalScrollBar();
    if (this->direction) return scroll->value()> scroll->maximum()*0.8;
    return scroll->value() < scroll->maximum()*0.2;
}

void ListView::wheelEvent(QWheelEvent *e)
{
    if (IsNeedUpdate() && isUpdated)
    {
        this->isUpdated = false;
        emit NeedUpdate();
    }
    auto newDelta = e->delta() < 0 ? -1 *20 :20;
    QListWidget::wheelEvent(new QWheelEvent(e->posF(),newDelta,e->buttons(),e->modifiers(),Qt::Vertical));
}

