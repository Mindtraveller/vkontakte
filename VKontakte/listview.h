#ifndef LISTVIEW_H
#define LISTVIEW_H

#include <QObject>
#include <QDebug>
#include <QWheelEvent>
#include <QListWidget>
#include <QScrollBar>


class ListView : public QListWidget
{
    Q_OBJECT
    bool isUpdated;
    bool direction; // true - DOWN; false - UP
    void wheelEvent(QWheelEvent *e);
public:
    ListView(QWidget *frame);
    ~ListView();

    void SetUpdated();
    void SetDirection(bool direction);
    bool IsNeedUpdate();
signals:
    void NeedUpdate();
};

#endif // LISTVIEW_H
