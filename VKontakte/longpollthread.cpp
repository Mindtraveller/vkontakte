#include "longpollthread.h"

LongPollThread::LongPollThread()
{

}

void LongPollThread::run()
{
    QJsonArray json;
    QJsonValue value;
    while(isRun)
    {
        json = RequestManager::LongPollServerRequest();
        if (!json.empty())  //here update
            foreach (value, json){
                auto jArray = value.toArray();
                qDebug() << "____New Event:" << jArray;
                auto event = jArray.first().toInt();
                emit Update(jArray,event);
            }
    }
}

LongPollThread::~LongPollThread()
{

}

