#ifndef LONGPOLLTHREAD_H
#define LONGPOLLTHREAD_H

#include "basethread.h"

enum UpdateTypes{
    NewMessage = 4,
    UsersActivity = 61, //type a message
    Online = 8,
    Offline = 9,
    MessageRead = 7,
    MessageRead_2 = 6   //own read
};

class LongPollThread : public BaseThread
{
public:
    LongPollThread();
    void run();
    ~LongPollThread();
//signals:
    //void Update(QJsonObject, UpdateTypes);
};

#endif // LONGPOLLTHREAD_H
