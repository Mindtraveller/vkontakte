#include <QApplication>
#include "StartWindow.h"
#include "Credentials.h"
#include "RequestManager.h"
#include "MainWindow.h"
#include "threadmanager.h"

int main(int argc, char *argv[])
{
	auto app = new QApplication(argc,argv);

	auto credentials = new Credentials();
	if (!credentials->IsValid())
	{
		auto loginWindow = new StartWindow();
		loginWindow->Show();
		app->exec();
		auto userId = loginWindow->GetUserId();
		auto accessToken = loginWindow->GetAccessToken();
		delete loginWindow;
		credentials = new Credentials(userId, accessToken);
		credentials->SaveToFile();
		RequestManager::Init(userId, accessToken);
	}

	qDebug() << "Id: " + credentials->GetUserId();
	qDebug() << "aToken: " + credentials->GetAccessToken();

    ThreadManager::Init();
    MainWindow window;
    window.show();
    ThreadManager::GetInstance()->StartThreads();

	return app->exec();
}


