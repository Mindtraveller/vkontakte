#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->loadingProgressBar->hide();
    SetConversations();
    HideRemoveForwardButton();

    on_actionOnline_triggered();

    this->mutexForConversations = new QMutex();
    QObject::connect(ui->conversations,SIGNAL(itemClicked(QListWidgetItem*)),
                     this,SLOT(ShowMessageHistory(QListWidgetItem*)));
    QObject::connect(ui->friendsList,SIGNAL(itemClicked(QListWidgetItem*)),
                     this,SLOT(ShowUsersPage(QListWidgetItem*)));

    QObject::connect(ui->dialog,SIGNAL(itemSelectionChanged()),this,SLOT(ShowForwardButton()));
    QObject::connect(ui->messageInput,SIGNAL(MessageSent()),this,SLOT(OnMessageSent()));

    QObject::connect(ui->conversations,SIGNAL(NeedUpdate()),this,SLOT(OnConversationsScroll()));
    QObject::connect(ui->newsFrame,SIGNAL(NeedUpdate()),this,SLOT(OnNewsScroll()));
    QObject::connect(ui->dialog,SIGNAL(NeedUpdate()),this,SLOT(OnDialogScroll()));
    QObject::connect(ui->friendsList,SIGNAL(NeedUpdate()),this,SLOT(OnFriendsScroll()));

    QObject::connect(ThreadManager::GetInstance(),SIGNAL(NewMessage(QJsonObject)),
                                    this,SLOT(OnNewMessage(QJsonObject)));

    QObject::connect(ThreadManager::GetInstance(),SIGNAL(UsersActivity(QJsonArray)),
                                    this,SLOT(OnUsersActivity(QJsonArray)));
    QObject::connect(ThreadManager::GetInstance(),SIGNAL(ChangedUsersState(int)),
                     this,SLOT(OnChangedUsersState(int)));
    QObject::connect(ThreadManager::GetInstance(),SIGNAL(MessageRead(int)),
                     this,SLOT(OnMessageRead(int)));
    ui->dialog->SetDirection(false);

    ShowNewsFrame();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::OnMessageSent()
{
    HideRemoveForwardButton();
}

void MainWindow::ProgressBarTick(double percent)
{
    int nextValue = ui->loadingProgressBar->maximum() * percent;
    ui->loadingProgressBar->setValue(nextValue == 0 ? percent * ui->loadingProgressBar->maximum() : nextValue);
}

//FRIENDS

void MainWindow::OnFriendsScroll()
{
    RequestManager::GetFriendsAsync(this,QString::number(ui->friendsList->count()));
}

void MainWindow::AddFriends(QJsonArray json)
{
     foreach (auto _friend, json)
         ShowFriend(_friend.toObject());
}

void MainWindow::ShowFriend(QJsonObject json)
{
    auto listItem = new QListWidgetItem();
    auto widget = new QWidget();
    auto hLayout = new QHBoxLayout();
    auto vLayout = new QVBoxLayout();
    auto nameLabel = new QLabel(json.value("first_name").toString()+ " "+
                                   json.value("last_name").toString());
    nameLabel->setStyleSheet("font:bold;");
    auto photoLabel = CreatePhotoLabel(ImageManager::ProcessImage(json.value("photo_medium").toString(),
                                                                  json.value("uid").toInt()));
    hLayout->addWidget(photoLabel);
    if (json.value("online").toInt() == 1)
    {
        auto layout = new QHBoxLayout();
        layout->addWidget(nameLabel);
        auto onlineLabel = new QLabel("Online");
        layout->addWidget(onlineLabel);
        vLayout->addLayout(layout);
        vLayout->setAlignment(onlineLabel,Qt::AlignLeft);
        onlineLabel->setContentsMargins(10,0,0,0);
    }else vLayout->addWidget(nameLabel);

    foreach (auto value, Ui::fieldNamesForUserPage) {
        if(json.value(value).toString() != "")
        {
            auto fieldValue = new QLabel(json.value(value).toString());
            fieldValue->setTextInteractionFlags(Qt::TextSelectableByMouse);
            vLayout->addWidget(fieldValue);
        }
    }
    vLayout->setContentsMargins(10,0,0,0);
    hLayout->addLayout(vLayout);

    auto button = new QPushButton("Write a message");

    hLayout->addWidget(button);
    hLayout->setAlignment(button,Qt::AlignTop);
    hLayout->setContentsMargins(1,1,1,1);
    hLayout->setSpacing(1);
    widget->setLayout(hLayout);
    widget->setCursor(Qt::PointingHandCursor);
    listItem->setSizeHint(widget->sizeHint());
    listItem->setData(Qt::UserRole,QString::number(json.value("uid").toInt())+ "&" + nameLabel->text());
    connect(button, &QPushButton::clicked, [this,listItem](){
        this->ShowMessageHistory(listItem);
    });
    ui->friendsList->addItem(listItem);
    ui->friendsList->setItemWidget(listItem, widget);
}

//USER PAGE

void MainWindow::ShowUsersPage(QListWidgetItem *item)
{
    ShowUsersPageFrame();
    auto json = RequestManager::GetUserInfo(item->data(Qt::UserRole).toString().split("&")[0]);

    auto mainLayout = new QBoxLayout(QBoxLayout::TopToBottom);

    mainLayout->addWidget(CreateUserInfo(json));


    auto photos = RequestManager::GetProfilePhotos(item->data(Qt::UserRole).toString().split("&")[0].toInt());

    auto photoLayout = new QHBoxLayout();
    auto photoListView = new ListView(this);
    photoListView->setFlow(QListWidget::LeftToRight);
    photoLayout->addWidget(photoListView);
    auto photoWidget = new QWidget();
    foreach (auto photo, photos) {
        auto objPhoto = photo.toObject();
        auto layout = new QHBoxLayout();
        auto widget = new QWidget();
        auto listItem = new QListWidgetItem();
        auto photoLabel = CreatePhotoLabel(ImageManager::ProcessImage(objPhoto.value("src").toString(),
                                                                      objPhoto.value("owner_id").toInt()));
        layout->addWidget(photoLabel);
        widget->setLayout(layout);
        widget->setCursor(Qt::PointingHandCursor);
        listItem->setSizeHint(widget->sizeHint());
        photoListView->insertItem(photoListView->count(),listItem);
        photoListView->setItemWidget(listItem,widget);
    }

    photoWidget->setLayout(photoLayout);
    mainLayout->addWidget(photoWidget);

    ui->userInfoFrame->setLayout(mainLayout);

}

QWidget* MainWindow::CreateUserInfo(QJsonObject json)
{
    auto infoWidget = new QWidget();
    auto infoHLayout = new QHBoxLayout();
    auto infoLayout = new QVBoxLayout();
    auto nameLabel = new QLabel(json.value("first_name").toString() +
                                " " + json.value("last_name").toString());
    infoLayout->addWidget(nameLabel);

    auto photoLabel = CreatePhotoLabel(
                ImageManager::ProcessImage(
                    json.value("photo_200_orig").toString(), json.value("uid").toInt()),1);
    infoHLayout->addWidget(photoLabel);

    auto statusLabel = new QLabel(json.value("status").toString());
    statusLabel->setWordWrap(true);
    infoLayout->addWidget(statusLabel);

    infoHLayout->addLayout(infoLayout);

    foreach (auto value, Ui::fieldNamesForUserPage) {
        if(json.value(value).toString() != "")
        {
            auto layout = new QHBoxLayout();
            auto realFieldName = new QLabel(Ui::realNamesForUserPage.at(Ui::fieldNamesForUserPage.indexOf(value)));
            auto fieldValue = new QLabel(json.value(value).toString());
            fieldValue->setTextInteractionFlags(Qt::TextSelectableByMouse);
            layout->addWidget(realFieldName);
            layout->addWidget(fieldValue);
            infoLayout->addLayout(layout);
        }
    }

    infoWidget->setLayout(infoHLayout);
    infoWidget->setFixedHeight(photoLabel->width());
    return infoWidget;
}

//CONVERSATIONS methods

void MainWindow::SetConversations()
{
    ui->conversations->clear();
    auto json = RequestManager::GetDialogs();
    ThreadManager::GetInstance()->Cache(json);
    AddConversations(json);
}

void MainWindow::AddConversations(QJsonArray json, int position)
{
    foreach (auto jsonItem, json)
    {
        auto message = jsonItem.toObject();
        auto listItem = new QListWidgetItem();
        auto widget = new QWidget();
        if (message.contains("chat_id"))
        {
            widget = CreateConversationWidgetForChat(message);
            listItem = CreateListItemForChatConversations(widget,message);
        }
        else
        {
            widget = CreateConversationWidget(message);
            listItem = CreateListItemForConversations(widget,message);
        }
        ui->conversations->insertItem(position == -1 ? ui->conversations->count():position, listItem);
        ui->conversations->setItemWidget(listItem, widget);
    }
}

QWidget *MainWindow::CreateConversationWidget(QJsonObject message)
{
    auto widget = new QWidget();
    auto hLayout = new QHBoxLayout();
    auto vLayout = new QVBoxLayout();
    auto person = RequestManager::GetUserInfo(QString::number(message.value("uid").toInt()));
    auto userName = CreateNameLabel(GetUserName(message));
    if (message.value("read_state").toInt() == 0 && message.value("out").toInt() == 0)
    {
        auto tempLayout = new QHBoxLayout();
        tempLayout->addWidget(userName);
        auto newMessLabel = new QLabel(QString("•"));
        newMessLabel->setStyleSheet("font:bold;color:#FFCC00;");
        tempLayout->addWidget(newMessLabel);
        tempLayout->setAlignment(Qt::AlignLeft);
        vLayout->addLayout(tempLayout);
    }
    else vLayout->addWidget(userName);

    auto messageLabel = new QLabel(message.value("body").toString() == "" ?
                                       "_attachments_":message.value("body").toString());
    messageLabel->setStyleSheet(message.value("read_state").toInt() == 0 ? Ui::unreadMessage:Ui::message);

    if (message.value("out").toInt() == 1 )
    {
        auto photoLayout = new QHBoxLayout();
        auto avatarLabel = CreatePhotoLabel(
                    ImageManager::ProcessImage(RequestManager::GetUserInfo().value("photo_50").toString(),
                                               RequestManager::GetCurrentUser().toInt()),2);
        photoLayout->addWidget(avatarLabel);
        photoLayout->addWidget(messageLabel);
        vLayout->addLayout(photoLayout);
    }else vLayout->addWidget(messageLabel);

    auto photoLabel = CreatePhotoLabel(
                ImageManager::ProcessImage(person.value("photo_50").toString(),
                                           person.value("uid").toInt()));
    hLayout->addWidget(photoLabel);

    hLayout->addLayout(vLayout);
    hLayout->setContentsMargins(0,0,0,0);
    widget->setLayout(hLayout);
    widget->setCursor(Qt::PointingHandCursor);
    widget->setStyleSheet(person.value("online").toInt() == 1? Ui::conversationItemOnline: Ui::conversationItem);
    return widget;
}

QWidget* MainWindow::CreateConversationWidgetForChat(QJsonObject message)
{
    auto widget = new QWidget();
    auto hLayout = new QHBoxLayout();
    auto vLayout = new QVBoxLayout();
    auto person = RequestManager::GetUserInfo(QString::number(message.value("uid").toInt()));

    auto chatName = CreateNameLabel(message.value("title").toString());
    vLayout->addWidget(chatName);

    auto messageLabel = new QLabel(message.value("body").toString() == "" ?
                                       "_attachments_":message.value("body").toString());
    messageLabel->setStyleSheet(message.value("read_state").toInt() == 0 ? Ui::unreadMessage: Ui::message);

    auto photoLayout = new QHBoxLayout();
    auto userPhotoLabel = CreatePhotoLabel(ImageManager::ProcessImage(person.value("photo_50").toString(),
                                                                  person.value("uid").toInt()),2);
    photoLayout->addWidget(userPhotoLabel);
    photoLayout->addWidget(messageLabel);
    vLayout->addLayout(photoLayout);

    auto chatPhotoLabel = CreatePhotoLabel(ImageManager::ProcessImage(message.value("photo_100").toString(),
                                                                  person.value("chat_id").toInt()),2);
    hLayout->addWidget(chatPhotoLabel);

    hLayout->addLayout(vLayout);
    hLayout->setContentsMargins(0,0,0,0);
    widget->setLayout(hLayout);
    widget->setStyleSheet(Ui::conversationItem);
    widget->setCursor(Qt::PointingHandCursor);
    return widget;
}

void MainWindow::OnConversationsScroll()
{
    RequestManager::GetDialogsAsync(this,QString::number(ui->conversations->count()));
}

void MainWindow::OnChangedUsersState(int userId)
{
    for (auto i =0; i< ui->conversations->count(); i++)
        if (ui->conversations->item(i)->data(Qt::UserRole).toString().split('&')[0].toInt() == userId)
        {
            auto dialogs = RequestManager::GetDialogs(QString::number(i),QString::number(ui->conversations->count()));
            foreach (auto dialog, dialogs) {
                auto jDialog = dialog.toObject();
                if (jDialog.value("uid").toInt() == userId)
                {
                    auto listItem = ui->conversations->item(i);
                    auto newWidget = CreateConversationWidget(jDialog);
                    ui->conversations->setItemWidget(listItem, newWidget);
                    break;
                }
            }
            break;
        }
}

void MainWindow::OnMessageRead(int conversationId)
{
    for (auto i =0; i< ui->conversations->count(); i++)
    {
        auto dialogId = ui->conversations->item(i)->data(Qt::UserRole).toString().split('&')[0].toInt();
        auto chatId = ui->conversations->item(i)->data(Qt::UserRole).toString().split('$')[0].toInt();
        if (dialogId == conversationId || chatId== conversationId - 2000000000 )// because chat_id is not valid in result of request
        {
            auto listItem = new QListWidgetItem();
            auto newWidget = new QWidget();
            auto dialogs = RequestManager::GetDialogs(QString::number(i),QString::number(ui->conversations->count()));
            foreach (auto dialog, dialogs) {
                auto jDialog = dialog.toObject();
                if (jDialog.value("uid").toInt() == conversationId)
                {
                    listItem = ui->conversations->item(i);
                    newWidget = CreateConversationWidget(jDialog);
                    break;
                }
                else if (jDialog.value("chat_id").toInt() ==  conversationId - 2000000000 )
                {
                    listItem = ui->conversations->item(i);
                    newWidget = CreateConversationWidgetForChat(jDialog);
                    break;
                }
            }
            ui->conversations->setItemWidget(listItem, newWidget);
        }
    }
}

//DIALOGS methods

void MainWindow::ShowMessageHistory(QListWidgetItem *item)
{
    QJsonArray json;
    QString someId;
    if (item->data(Qt::UserRole).toString().contains("&")){
        auto temp = item->data(Qt::UserRole).toString().split('&');
        someId = temp[0];
        qDebug() << "____User ID: " + someId;
        ui->companionNameLabel->setText(temp[1]);
        ShowDialogFrame("user_id=" + someId);
        json = RequestManager::GetMessageHistory(someId);
        AddDialogItem(json, true);

    }else{
        auto temp = item->data(Qt::UserRole).toString().split('$'); // $ in chat !
        someId = temp[0];
        qDebug() << "____Chat ID: " + someId;
        ui->companionNameLabel->setText(temp[1]);
        ShowDialogFrame("chat_id=" + someId);
        json = RequestManager::GetChatMessageHistory(someId);
        AddDialogItem(json, true);
    }
}

void MainWindow::AddDialogItem(QJsonArray json, bool first)
{
    QJsonValue value;
    auto userId = RequestManager::GetCurrentUser();
    foreach (value, json) {
           auto jObject = value.toObject();
           auto listItem = new QListWidgetItem();
           auto widget = new QWidget();
           if (QString::number((int)jObject.value("from_id").toInt()) == userId)
           {
               widget = CreateDialogWidget(&jObject,true);
           }else
           {
               widget = CreateDialogWidget(&jObject,false);
           }
           listItem = CreateListItemForDialogs(widget, &jObject);
           ui->dialog->insertItem(first ? ui->dialog->count(): 0,listItem);
           ui->dialog->setItemWidget(listItem, widget);
           if (first) ui->dialog->scrollToBottom();
    }
}

void MainWindow::ShowAttachments(QVBoxLayout *layout, QJsonArray attachments)
{
    //qDebug() << attachments;
    foreach (auto attach, attachments) {
        auto label = new QLabel();
        auto jObject = attach.toObject();
       // qDebug() << "____message attach___" << jObject;
        if (!jObject.value("doc").toObject().isEmpty()){    //DOC
            jObject = jObject.value("doc").toObject();
            label = CreateHyperLink(jObject.value("url").toString(),jObject.value("title").toString());
            label->setStyleSheet("font-size:12px;");
            layout->addWidget(label);
        } else if (!jObject.value("photo").toObject().isEmpty()){   //PHOTO
            jObject = jObject.value("photo").toObject();
            label = CreatePhotoLabel(jObject,2);
            layout->addWidget(label);
        }
    }
}

void MainWindow::ShowForwardMessages(QVBoxLayout *layout, QJsonArray messages, bool isMy)
{
    foreach (auto message, messages) {
        auto messageObject = message.toObject();
        auto hBox = new QHBoxLayout();
        auto person = RequestManager::GetUserInfo(messageObject.value("uid").toInt());
        auto userName = GetUserName(messageObject);
        auto photoLabel = CreatePhotoLabel(ImageManager::ProcessImage(person.value("photo_50").toString(),
                                                                      person.value("uid").toInt()),2);
        photoLabel->setFixedWidth(50);  //WTF
        hBox->addWidget(photoLabel);
        //hBox->setAlignment(photoLabel,Qt::AlignTop);

        auto vBox = new QVBoxLayout();

        auto nameLabel = CreateNameLabel(userName);
        vBox->addWidget(nameLabel);
        vBox->setAlignment(nameLabel,Qt::AlignTop);

        auto messageLabel = CreateDialogWidget(&messageObject, !isMy);
        messageLabel->setStyleSheet(isMy ? Ui::dialogItemMessageMy + "margin-left:10px;": Ui::dialogItemMessage);
        vBox->addWidget(messageLabel);

        hBox->addLayout(vBox);
        layout->addLayout(hBox);
    }
}

QWidget* MainWindow::CreateDialogWidget(QJsonObject *message, bool isMy)
{
    //qDebug() << "_____message" << *message;
    QWidget* widget = new QWidget();
    QVBoxLayout* layout = new QVBoxLayout(widget);

    auto dateLabel = CreateDateLabel(*message);

    auto messageLabel = CreateMessageLabel(message);
    layout->addWidget(messageLabel);

    if (message->contains("attachments")) ShowAttachments(layout, message->value("attachments").toArray());
    if (message->contains("fwd_messages")) ShowForwardMessages(layout, message->value("fwd_messages").toArray(), isMy);

    layout->addWidget(dateLabel);
    widget->setLayout(layout);
    widget->setStyleSheet(isMy ? Ui::dialogItemMessageMy: Ui::dialogItemMessage);

    auto HLayout = new QHBoxLayout();
    if (!isMy && message->contains("chat_id"))
    {
        auto person = RequestManager::GetUserInfo(QString::number(message->contains("from_id") ?
                                                                      message->value("from_id").toInt() :
                                                                      message->value("uid").toInt()));
        auto photoLabel = CreatePhotoLabel(ImageManager::ProcessImage(person.value("photo_50").toString(),
                                                                      person.value("uid").toInt()));
        photoLabel->setFixedWidth(100);  //WTF
        HLayout->addWidget(photoLabel);
    }

    HLayout->addWidget(widget);
    auto mainWidget = new QWidget();
    mainWidget->setLayout(HLayout);
    mainWidget->setStyleSheet(isMy ? Ui::dialogItemMy : Ui::dialogItem);
    mainWidget->setCursor(Qt::PointingHandCursor);
    if (message->value("read_state").toInt() == 0)
        RequestManager::MarkMessagesAsRead(message->value("mid").toInt());
    return mainWidget;
}

void MainWindow::OnDialogScroll()
{
    auto prevValue = ui->dialog->verticalScrollBar()->value();
    auto prevMax = ui->dialog->verticalScrollBar()->maximum();
    auto recipient = ui->messageInput->GetRecipient().split('=');   //0 - method; 1 - ID
    if (recipient[0] == "user_id") AddDialogItem(RequestManager::GetMessageHistory(recipient[1],
                                                 QString::number(ui->dialog->count()),false),false);
    else AddDialogItem(RequestManager::GetChatMessageHistory(recipient[1],
                       QString::number(ui->dialog->count()),false),false);

    auto currentMax = ui->dialog->verticalScrollBar()->maximum();
    auto prevPerc = (currentMax-prevMax)/prevMax;
    auto newPerc = (currentMax-prevMax)/currentMax;
    ui->dialog->verticalScrollBar()->setValue(
                (currentMax-prevMax)+prevValue-prevValue*(prevPerc-newPerc));  // improve if posible

    SetDialogUpdated();
}

void MainWindow::OnNewMessage(QJsonObject message)
{
    qDebug() << "_____New message: " << message;
    ui->activityLabel->clear();
    int recipient;
    if (message.contains("chat_id"))
        recipient = message.value("chat_id").toInt();
    else recipient = message.value("uid").toInt();

    //Update dialog

    if (ui->dialogFrame->isVisible() && (recipient == RequestManager::GetCurrentUser().toInt() ||
            recipient == ui->messageInput->GetRecipient().split('=')[1].toInt()))
    {
        ui->activityLabel->setText("");
        auto widget = CreateDialogWidget(&message, message.value("out").toInt() == 0 ? false: true);
        auto listItem = CreateListItemForDialogs(widget, &message);
        ui->dialog->insertItem(ui->dialog->count(),listItem);
        ui->dialog->setItemWidget(listItem, widget);
        ui->dialog->scrollToBottom();

        if (message.value("out").toInt() == 1) RequestManager::MarkMessagesAsRead(message.value("mid").toInt());
    }


    //Update Conversations
    auto dialogs = RequestManager::GetDialogs("0","10");
    QJsonObject dialog;
    foreach (auto someDialog, dialogs) {
        auto jSome = someDialog.toObject();
        if (jSome.value("uid").toInt() == recipient || jSome.value("chat_id").toInt() == recipient )
        {
            dialog = jSome;
            break;
        }
    }
    auto strRecipient = QString::number(recipient);

    auto listItem = new QListWidgetItem();
    auto widget = new QWidget();
    if (message.contains("chat_id"))
    {
        widget = CreateConversationWidgetForChat(dialog);
        listItem = CreateListItemForChatConversations(widget,dialog);
    }
    else
    {
        widget = CreateConversationWidget(dialog);
        listItem = CreateListItemForConversations(widget,dialog);
    }
    ui->conversations->insertItem(0,listItem);
    ui->conversations->setItemWidget(listItem,widget);

    auto isFound = false;
    for (auto i =0; i< ui->conversations->count(); i++)
        if (ui->conversations->item(i)->data(Qt::UserRole).toString().split('&')[0] == strRecipient ||
                ui->conversations->item(i)->data(Qt::UserRole).toString().split('$')[0] == strRecipient)    //for chat!
        {
            if (isFound)
            {
                ui->conversations->takeItem(i);
                break;
            }
            isFound = true;
        }
}

void MainWindow::OnUsersActivity(QJsonArray json)
{
    if (ui->dialogFrame->isVisible() && json[1].toInt() == ui->messageInput->GetRecipient().split('=')[1].toInt())
    {
        auto userInfo = RequestManager::GetUserInfo(json[1].toInt());
        ui->activityLabel->setText(userInfo.value("first_name").toString()+ " " +
                                   userInfo.value("last_name").toString() + " is typing...");
        auto thread = ThreadManager::GetSleepThread();
        thread->SetSleepTime(5000);
        QObject::connect(thread,&SleepThread::Done,[this](){
            ui->activityLabel->clear();
        });
        ThreadManager::GetInstance()->RunSleepThread(thread);
    }
}

//NEWS

void MainWindow::CreateNewsItem(QJsonObject json, QJsonObject owner)
{
   // qDebug() << "____News:" << json;
    auto listItem = new QListWidgetItem();
    auto widget = new QWidget();
    auto contentLayout = new QVBoxLayout();
    auto hLayout = new QHBoxLayout();

    auto textLabel = new QLabel(json.value("text").toString());

    auto nameString = owner.contains("name") ? owner.value("name").toString() :
                                               owner.value("first_name").toString()+ " " +
                                               owner.value("last_name").toString();
    auto nameLabel = new QLabel("<b>"+ nameString + "</b>");
    auto groupPhoto = CreatePhotoLabel(ImageManager::ProcessImage(owner.value("photo").toString(),
                                       owner.contains("gid") ? owner.value("gid").toInt() : owner.value("uid").toInt()));
    hLayout->addWidget(groupPhoto);
    hLayout->setAlignment(groupPhoto,Qt::AlignTop);
    contentLayout->addWidget(nameLabel);
    contentLayout->setAlignment(nameLabel,Qt::AlignTop);

    textLabel->setWordWrap(true);
    contentLayout->addWidget(textLabel);
    auto attachment = json.value("attachment").toObject();
    if(attachment.contains("link"))
    {
        auto linkJson = attachment.value("link").toObject();
        auto linkTitle = CreateHyperLink(linkJson.value("url").toString(),linkJson.value("title").toString());
        auto linkDesc = new QLabel(linkJson.value("description").toString());
        linkTitle->setWordWrap(true);
        linkDesc->setWordWrap(true);
        contentLayout->addWidget(linkTitle);
        if (linkJson.contains("image_big"))
        {
            QJsonObject temp;   //for using already existing method
            temp.insert("src_big",linkJson.value("image_big").toString());
            temp.insert("owner_id","0");
            auto photoLabel = CreatePhotoLabel(temp);
            contentLayout->addWidget(photoLabel);
            contentLayout->setAlignment(photoLabel,Qt::AlignCenter);
        }
        contentLayout->addWidget(linkDesc);
    }
    if (attachment.contains("photo"))
    {
        auto photoLabel = CreatePhotoLabel(attachment.value("photo").toObject());
        contentLayout->addWidget(photoLabel);
        contentLayout->setAlignment(photoLabel,Qt::AlignCenter);
    }

    contentLayout->addLayout(NewsPostInfo(json));
    contentLayout->setContentsMargins(10,0,0,0);
    hLayout->addLayout(contentLayout);
    hLayout->setContentsMargins(10,5,50,5);
    hLayout->setSpacing(5);

    widget->setLayout(hLayout);
    widget->setCursor(Qt::PointingHandCursor);
    listItem->setSizeHint(widget->sizeHint());
    //listItem->setData(Qt::UserRole,QString::number(message.value("uid").toInt()));    //mb post ID
    ui->newsFrame->addItem(listItem);
    ui->newsFrame->setItemWidget(listItem, widget);
}

QHBoxLayout *MainWindow::NewsPostInfo(QJsonObject json) //likes, reposts, date
{
    auto postInfo = new QHBoxLayout();
    postInfo->setContentsMargins(0,10,0,0);
    auto dateLabel = CreateDateLabel(json);
    postInfo->addWidget(dateLabel);
    postInfo->setAlignment(dateLabel,Qt::AlignLeft);

    auto likesObj = json.value("likes").toObject();
    auto repostsObj = json.value("reposts").toObject();
    auto likesLabel = new QLabel(QString::number(likesObj.value("count").toInt()));
    auto repostsLabel = new QLabel(QString::number(repostsObj.value("count").toInt()));

    this->likes.append(json.value("likes").toObject().value("user_likes").toInt());
    auto likesIndex = this->likes.length()-1;

    auto some = new QHBoxLayout();
    some->setSizeConstraint(QLayout::SetFixedSize);
    auto likeButton = new QPushButton(this->likes[likesIndex] ==0 ? QIcon(Ui::likeIcon) : QIcon(Ui::likeInverseIcon) ,"");
    likeButton->setFixedSize(20,20);
    likeButton->setIconSize(QSize(20,20));

    auto isBusy = new QMutex();
    connect(likeButton, &QPushButton::pressed, [=] (){
        if (!isBusy->tryLock()) return;
        auto likeValue = likesLabel->text().toInt();
        auto ownerId = json.value("source_id").toInt();
        qDebug() << this->likes[likesIndex];
        if (this->likes[likesIndex] == 0){
            likeValue = RequestManager::LikePost(ownerId,
                                     json.value("post_id").toInt(),
                                     json.value("access_key").toString()).value("likes").toInt();
        }
        else{
            likeValue = RequestManager::DislikePost(ownerId,
                                        json.value("post_id").toInt(),
                                        json.value("access_key").toString()).value("likes").toInt();
        }
        if (this->likes[likesIndex] == 0) likeButton->setIcon(QIcon(Ui::likeInverseIcon));
        else likeButton->setIcon(QIcon(Ui::likeIcon));
        if (this->likes[likesIndex] == 0) this->likes[likesIndex] = 1;
        else this->likes[likesIndex] = 0;
        qDebug() << this->likes[likesIndex];

        likesLabel->setText(QString::number(likeValue));
        isBusy->unlock();
    });

    auto repostButton = new QPushButton(QIcon(Ui::repostIcon),"");
    repostButton->setFixedSize(20,20);
    repostButton->setIconSize(QSize(20,20));

    connect(repostButton, &QPushButton::pressed,[repostsLabel](){
            //TODO: repost to wall
    });
    some->addWidget(repostButton);
    some->addWidget(repostsLabel);
    some->addWidget(likeButton);
    some->addWidget(likesLabel);
    some->setAlignment(likesLabel,Qt::AlignLeft);
    some->setAlignment(likeButton,Qt::AlignRight);
    some->setAlignment(repostsLabel,Qt::AlignLeft);
    some->setAlignment(repostButton,Qt::AlignRight);
    postInfo->addLayout(some);
    return postInfo;
}

void MainWindow::AddNews(QJsonObject json)
{
   // qDebug() << "____Full newsfeed" << json;
    auto items = json.value("items").toArray();
    auto profiles = json.value("profiles").toArray();
   // qDebug() << "___ newsfeed" << items;
    auto groups = json.value("groups").toArray();
    foreach (auto profile, profiles) groups.append(profile);
    //qDebug() << "____GroupFeed:" << groups;
    for(auto i =0 ;i< items.count(); i++)
    {
        auto objItem = items[i].toObject();
        auto sourceId = objItem.value("source_id").toInt();
        //qDebug() << objItem;
        sourceId*= sourceId < 0 ? -1 : 1;
        foreach (auto group, groups) {
            auto groupObj = group.toObject();
            if (groupObj.contains("gid") && groupObj.value("gid").toInt() == sourceId)
            {
                CreateNewsItem(objItem, groupObj);
                break;
            }
            else if (groupObj.contains("uid") && groupObj.value("uid").toInt() == sourceId)
            {
                CreateNewsItem(objItem, groupObj);
                break;
            }
        }
    }
}

void MainWindow::OnNewsScroll()
{
    RequestManager::GetNewsFeedAsync(this, QString::number(ui->newsFrame->count()));
}

//CLICK events

void MainWindow::on_actionExit_triggered()
{
	this->close();
}

void MainWindow::on_actionSign_out_triggered()
{
    (new Credentials())->DeleteFile();
    QProcess::startDetached(QApplication::applicationFilePath());
    this->close();
}

void MainWindow::on_actionOnline_triggered()
{
    ThreadManager::GetInstance()->SetOnline();
    this->setWindowIcon(QIcon(Ui::OnlineIcon));
}

void MainWindow::on_actionOffline_triggered()
{
    ThreadManager::GetInstance()->SetOffline();
    this->setWindowIcon(QIcon(Ui::OfflineIcon));
}

void MainWindow::on_friendsButton_clicked()
{
    ShowFriendsFrame();
    auto friends = RequestManager::GetFriends();
    AddFriends(friends);
}

void MainWindow::on_newsButton_clicked()
{
    this->likes.clear();
    ShowNewsFrame();
}

void MainWindow::on_forwardButton_clicked()
{
    auto list = ui->dialog->selectedItems();
    auto mIds = new QList<int>();
    ShowProgressBar();
    foreach (auto item, list) {
        mIds->append(item->data(Qt::UserRole).toInt());
    }
    ui->messageInput->SetForwardMessages(mIds);
    ui->conversations->clearSelection();
    ShowRemoveForwardButton();
    HideProgressBar();
}

void MainWindow::on_removeForwardButton_clicked()
{
    ui->messageInput->RemoveForwardMessages();
    HideRemoveForwardButton();
}

void MainWindow::on_addPhotoButton_clicked()
{
    auto imageNames = QFileDialog::getOpenFileNames(this, "Choose Photo", "/home",
                                                   "Images (*.png *.bmp *.jpg *.gif)");
    ShowProgressBar();
    auto photosCount = imageNames.length();
    foreach (auto imageName, imageNames) {

        if (imageName.isEmpty()) return;

        auto json = RequestManager::GetMessagesUploadServerForPhotos();
        auto result = RequestManager::PostImageToServer(json.value("upload_url").toString(),imageName);
        if (result.isEmpty()) return;
//        qDebug() << "_______" << result;
        auto photos = RequestManager::SaveMessagesPhoto(result);
        //qDebug() << photos;
        auto photoObj = photos.first().toObject();
        ui->messageInput->AddPhotoToMessage(photoObj.value("pid").toInt(),
                                            photoObj.value("owner_id").toInt());
        ProgressBarTick((double)imageNames.indexOf(imageName)/photosCount);
    }
    HideProgressBar();
}

void MainWindow::on_addDocumentButton_clicked()
{
    ShowProgressBar();
    auto documentNames = QFileDialog::getOpenFileNames(this, "Choose Docs", "/home", "");
    auto docsCount = documentNames.length();
    foreach (auto docName, documentNames) {

        if (docName.isEmpty()) return;

        auto server = RequestManager::GetDocsUploadServer();
        auto document = RequestManager::SaveDoc(server.value("upload_url").toString(),docName);
        //qDebug() << "______" << document;
        auto doc = document.first().toObject();
        ui->messageInput->AddDocToMessage(doc.value("did").toInt(), doc.value("owner_id").toInt());
        ProgressBarTick((double)documentNames.indexOf(docName)/docsCount);
    }
    HideProgressBar();
}

//SHOW methods

void MainWindow::ShowUsersPageFrame()
{
    ui->dialogFrame->hide();
    ui->friendsList->hide();
    ui->newsFrame->hide();
    ui->userInfoFrame->show();
    auto layout = ui->userInfoFrame->layout();
    QLayoutItem *item;
    if (layout != 0) while ((item = layout->takeAt(0)) != 0)
    {
        layout->removeWidget(item->widget());
        delete item->widget();
        delete item;
    }
    delete layout;
}

void MainWindow::ShowFriendsFrame()
{
    ui->dialogFrame->hide();
    ui->userInfoFrame->hide();
    ui->friendsList->show();
    ui->newsFrame->hide();
    ui->friendsList->clear();
}

void MainWindow::ShowDialogFrame(QString recipient)
{
    ui->userInfoFrame->hide();
    ui->friendsList->hide();
    ui->dialogFrame->show();
    ui->activityLabel->clear();
    ui->newsFrame->hide();
    ui->dialog->clear();
    ui->messageInput->setFocus();
    ui->messageInput->SetRecipient(recipient);
}

void MainWindow::ShowNewsFrame()
{
    ui->userInfoFrame->hide();
    ui->friendsList->hide();
    ui->dialogFrame->hide();
    ui->newsFrame->show();
    ui->newsFrame->clear();
    auto json = RequestManager::GetNewsFeed();
    AddNews(json);
}

void MainWindow::ShowForwardButton()
{
    if (ui->dialog->selectedItems().length() > 0 )
        ui->forwardButton->show();
    else ui->forwardButton->hide();
}

void MainWindow::ShowProgressBar()
{
    ui->loadingProgressBar->show();
    ui->messageInput->SetLoadState();
}

void MainWindow::HideProgressBar()
{
    ui->loadingProgressBar->hide();
    ui->loadingProgressBar->setValue(0);
    ui->messageInput->RemoveLoadState();
}

//SUPPORT methods

QLabel *MainWindow::CreateMessageLabel(QJsonObject *message)
{
    auto messageLabel = new QLabel(message->value("body").toString());
    messageLabel->setWordWrap(true);
    messageLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
    return messageLabel;
}

QLabel *MainWindow::CreateNameLabel(QString name)
{
    auto label = new QLabel(name);
    label->setStyleSheet("font:bold;");
    return label;
}

QListWidgetItem* MainWindow::CreateListItemForDialogs(QWidget *widget, QJsonObject* message )
{
    auto listItem = new QListWidgetItem();
    listItem->setSizeHint(widget->sizeHint());
    listItem->setData(Qt::UserRole, message->value("mid").toInt());
    return listItem;
}

QListWidgetItem* MainWindow::CreateListItemForConversations(QWidget *widget, QJsonObject message )
{
    auto listItem = new QListWidgetItem();
    listItem->setSizeHint(widget->sizeHint());
    auto userName = GetUserName(message);
    listItem->setData(Qt::UserRole,QString::number(message.value("uid").toInt())+ "&" + userName);
    return listItem;
}

QListWidgetItem* MainWindow::CreateListItemForChatConversations(QWidget *widget, QJsonObject message )
{
    auto listItem = new QListWidgetItem();
    listItem->setSizeHint(widget->sizeHint());
    auto chatName = message.value("title").toString();
    listItem->setData(Qt::UserRole,QString::number(message.value("chat_id").toInt())+ "$" + chatName);  //must be $ for chat
    return listItem;
}

QString MainWindow::GetUserName(QJsonObject json)
{
    auto person = RequestManager::GetUserInfo(QString::number(json.value("uid").toInt()));
    return person.value("first_name").toString()+ " "+ person.value("last_name").toString();
}

QLabel *MainWindow::CreateHyperLink(QString url, QString text)
{
    auto label = new QLabel();
    label->setText("<a href=\""+url+"\"><b>"+text+"</b></a>");
    label->setTextFormat(Qt::RichText);
    label->setTextInteractionFlags(Qt::TextBrowserInteraction);
    label->setOpenExternalLinks(true);
    return label;
}

QLabel *MainWindow::CreatePhotoLabel(QJsonObject json, int scale)
{
    auto url = ImageManager::ProcessImage(json.value("src_big").toString(),
                                          json.value("owner_id").toInt());
    return CreatePhotoLabel(url,scale);
}

QLabel *MainWindow::CreatePhotoLabel(QString url, int scale)    //size =size /scale
{
    auto photoLabel = new QLabel();
    QPixmap pixmap(url);
    pixmap = pixmap.scaled(pixmap.width()/scale,pixmap.height()/scale);
    photoLabel->setPixmap(pixmap);
    photoLabel->setFixedWidth(pixmap.width());
    photoLabel->setFixedHeight(pixmap.height());
    photoLabel->setScaledContents(true);
    return photoLabel;
}

QLabel *MainWindow::CreateDateLabel(QJsonObject json)
{
    auto dateTime = new QDateTime();
    dateTime->setTime_t(json.value("date").toInt());
    auto dateLabel = new QLabel(dateTime->toString(Qt::SystemLocaleShortDate));
    dateLabel->setStyleSheet(Ui::dialogDate);
    return dateLabel;
}

void MainWindow::ShowRemoveForwardButton()
{
    ui->removeForwardButton->show();
    ui->forwardButton->hide();
}

void MainWindow::HideRemoveForwardButton()
{
    ui->removeForwardButton->hide();
    ui->forwardButton->hide();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    auto geometry = ui->mainFrame->geometry();
    auto winSize = event->size();
    geometry.setX(ui->main_frame->x());
    geometry.setY(ui->main_frame->y());
    geometry.setWidth(winSize.width()- ui->conversations->width());
    geometry.setHeight(winSize.height() - ui->friendsButton->height()- 20);
    ui->main_frame->setGeometry(geometry);
    geometry.setX(ui->newsFrame->x());
    geometry.setY(ui->newsFrame->y());
    geometry.setWidth(winSize.width()- ui->conversations->width());
    ui->newsFrame->setGeometry(geometry);


    geometry.setX(ui->conversations->x());
    geometry.setY(ui->conversations->y());
    geometry.setWidth(ui->conversations->width());
    ui->conversations->setGeometry(geometry);
}

//For scroll async
void MainWindow::SetFriendsUpdated()
{
    ui->friendsList->SetUpdated();
}

void MainWindow::SetConversationsUpdated()
{
    ui->conversations->SetUpdated();
}

void MainWindow::SetNewsUpdated()
{
    ui->newsFrame->SetUpdated();
}

void MainWindow::SetDialogUpdated()
{
    ui->dialog->SetUpdated();
}

