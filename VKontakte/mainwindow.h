#pragma once
#include <QWidget>
#include <QMainWindow>
#include <QVBoxLayout>
#include <QDateTime>
#include <QVariant>
#include <QEvent>
#include <QList>
#include <QMutex>
#include <QListWidgetItem>
#include <QPushButton>
#include <QResizeEvent>
#include <QFileDialog>

#include "Credentials.h"
#include "ui_MainWindow.h"
#include "RequestManager.h"
#include "ImageManager.h"
#include "threadmanager.h"

namespace Ui {
    class MainWindow;
    const QString conversationItem = "margin:1px;padding:2px;";
    const QString conversationItemOnline = "background:#ADEBAD;"+conversationItem;
    const QString message = "margin:5px;padding:2px;";
    const QString unreadMessage = message +" background:lightblue;";
    const QString dialogItemMessageMy = "background:#E0F5FF;padding:5px 5px 0 5px;border-radius:20px;";
    const QString dialogItemMessage = dialogItemMessageMy + "background:#66C2FF;";
    const QString dialogItemMy = "margin: 0 0 0 50px;";
    const QString dialogItem = "margin: 0 50px 0px 0;";
    const QString dialogDate = "font-size:8px;font-style:italic;font-weight:bold;";

    enum ConversationType{
        Chat, User, ConversationName
    };

    const QList<QString> fieldNamesForUserPage = QList<QString>() << QString("home_town")
                                                                  << QString("mobile_phone")
                                                                  << QString("home_phone")
                                                                  << QString("skype")
                                                                  ;
    const QList<QString> realNamesForUserPage = QList<QString>()  << QString("Home town")
                                                                  << QString("Mobile phone")
                                                                  << QString("Home phone")
                                                                  << QString("Skype")
                                                                  ;
    const QString likeIcon = "icons/like.png";
    const QString repostIcon = "icons/repost.png";
    const QString repostInverseIcon = "icons/repostInverse.png";
    const QString likeInverseIcon = "icons/likeInverse.png";
    const QString OnlineIcon = "icons/online.png";
    const QString OfflineIcon = "icons/offline.png";
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Ui::MainWindow *ui;

    QList<int> likes;

    QMutex *mutexForConversations;

    //conversations
    QWidget *CreateConversationWidget(QJsonObject message);
    QWidget *CreateConversationWidgetForChat(QJsonObject message);

    //dialogs
    void ShowAttachments(QVBoxLayout *layout, QJsonArray attachments);
    void ShowForwardMessages(QVBoxLayout *layout, QJsonArray messages, bool isMy);
    void AddDialogItem(QJsonArray json, bool first = true);
    QWidget *CreateDialogWidget(QJsonObject* message, bool in);


    //friends
    void ShowFriend(QJsonObject json);

    //news
    QHBoxLayout *NewsPostInfo(QJsonObject json); //likes, reposts, date
    void CreateNewsItem(QJsonObject json, QJsonObject owner);

    //user page
    QWidget *CreateUserInfo(QJsonObject json);

    //show frame methods
    void ShowFriendsFrame();
    void ShowDialogFrame(QString recipient);
    void ShowNewsFrame();
    void ShowUsersPageFrame();

    //support methods
    QLabel *CreateNameLabel(QString name);
    QLabel *CreateMessageLabel(QJsonObject *message);
    QLabel *CreatePhotoLabel(QString url, int scale = 1);
    QLabel *CreatePhotoLabel(QJsonObject json, int scale=2);
    QLabel *CreateDateLabel(QJsonObject json);
    QLabel *CreateHyperLink(QString url, QString text);
    QListWidgetItem* CreateListItemForConversations(QWidget *widget, QJsonObject message );
    QListWidgetItem* CreateListItemForChatConversations(QWidget *widget, QJsonObject message );
    QListWidgetItem* CreateListItemForDialogs(QWidget *widget, QJsonObject *message );
    QString GetUserName(QJsonObject json);

    void ShowRemoveForwardButton();
    void HideRemoveForwardButton();

    void ShowProgressBar();
    void HideProgressBar();
    void ProgressBarTick(double percent);

    void resizeEvent(QResizeEvent *);

public:
	explicit MainWindow(QWidget *parent = nullptr);
    void AddConversations(QJsonArray json, int position = -1);
    void AddNews(QJsonObject json);
    void AddFriends(QJsonArray json);
    //methods for async scroll-load
    void SetConversationsUpdated();
    void SetNewsUpdated();
    void SetDialogUpdated();
    void SetFriendsUpdated();

	~MainWindow();
private slots:

    void ShowUsersPage(QListWidgetItem* item);

    void OnMessageSent();

    void SetConversations();
    void ShowMessageHistory(QListWidgetItem *item);

    void OnNewsScroll();
    void OnDialogScroll();
    void OnConversationsScroll();
    void OnFriendsScroll();

    void OnNewMessage(QJsonObject message);
    void OnChangedUsersState(int userId);
    void OnUsersActivity(QJsonArray json);
    void OnMessageRead(int dialogId);

    //menu buttons
    void on_actionExit_triggered();
    void on_actionSign_out_triggered();
    void on_actionOnline_triggered();
    void on_actionOffline_triggered();
    //

    void on_friendsButton_clicked();
    void on_newsButton_clicked();
    void on_forwardButton_clicked();
    void on_removeForwardButton_clicked();
    void on_addPhotoButton_clicked();
    void on_addDocumentButton_clicked();

    void ShowForwardButton();

};
