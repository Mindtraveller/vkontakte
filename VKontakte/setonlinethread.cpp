#include "setonlinethread.h"

SetOnlineThread::SetOnlineThread()
{

}

void SetOnlineThread::run()
{
    while(this->isRun)
    {
        RequestManager::SetOnline();
        QThread::sleep(850);    //sleep for 850 secs == 14,166 mins
    }
    RequestManager::SetOffline();   //when exit - set offline
}

SetOnlineThread::~SetOnlineThread()
{

}

