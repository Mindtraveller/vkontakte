#ifndef SETONLINETHREAD_H
#define SETONLINETHREAD_H

#include "basethread.h"

class SetOnlineThread : public BaseThread
{
public:
    SetOnlineThread();
    void run();
    ~SetOnlineThread();
};

#endif // SETONLINETHREAD_H
