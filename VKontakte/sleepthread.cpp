#include "sleepthread.h"

SleepThread::SleepThread()
{

}

SleepThread::~SleepThread()
{

}

void SleepThread::run()
{
    QThread::msleep(this->sleepTime);
    emit Done();
}

void SleepThread::SetSleepTime(int mTime)
{
    this->sleepTime = mTime;
}
