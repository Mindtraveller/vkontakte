#ifndef SLEEPTHREAD_H
#define SLEEPTHREAD_H

#include "basethread.h"

class SleepThread : public BaseThread
{
    int sleepTime;
public:
    void run();
    void SetSleepTime(int mTime);
    SleepThread();
    ~SleepThread();
};

#endif // SLEEPTHREAD_H
