#include "textedit.h"

TextEdit::TextEdit()
{
    this->setPlaceholderText("Type your message here...");
}

TextEdit::TextEdit(QWidget *frame):QTextEdit(frame)
{
    TextEdit();
}

TextEdit::~TextEdit()
{
}

void TextEdit::SetRecipient(QString userId)
{
    this->recipient = userId;
}

QString TextEdit::GetRecipient()
{
    return this->recipient;
}

void TextEdit::keyPressEvent(QKeyEvent *event)
{
    if((event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) &&
            !(event->modifiers() & Qt::ShiftModifier))
    {
        auto body = this->toPlainText();

        if (body.isEmpty() && this->forwardMessages.isEmpty() && this->attachments.isEmpty() || ! this->isLoading.tryLock()) return;

        this->clear();
        auto message = "&message=" + body;
        if (!this->forwardMessages.isEmpty())
        {
            message = message + "&forward_messages=" + ForwardMessagesToSring();
            RemoveForwardMessages();
        }
        if (!this->attachments.isEmpty())
        {
            message = message + "&attachment=" + AttachmentToString();
            RemoveAttachments();
        }

        RequestManager::SendMessage(this->recipient,message);
        emit MessageSent();
        this->isLoading.unlock();
    }
    else QTextEdit::keyPressEvent( event );
}

void TextEdit::SetForwardMessages(QList<int> *messageIds)
{
    this->forwardMessages = *messageIds;
}


void TextEdit::RemoveForwardMessages()
{
    this->forwardMessages.clear();
}

QString TextEdit::ForwardMessagesToSring()
{
    QString result = "";
    foreach (auto id, this->forwardMessages) {
        result = result + QString::number(id) + ",";
    }
    return result;
}

QString TextEdit::AttachmentToString()   //format <type><owner_id>_<media_id>
{
    QString result = "";
    foreach (auto pair, this->attachments) {
        result = result + pair.first + pair.second.first + "_" + pair.second.second + ",";
    }
    return result;
}


void TextEdit::AddPhotoToMessage(int photoId, int ownerId)
{
    if (this->attachments.length() == 10) return;  //TODO: show alert on MainWindow
    this->attachments.append(QPair<QString, QPair<QString, QString>>("photo",
                            QPair<QString,QString>(QString::number(ownerId < 0 ? -1 * ownerId : ownerId),
                                                   QString::number(photoId))));
}

void TextEdit::AddDocToMessage(int docId, int ownerId)
{
    if (this->attachments.length() == 10) return;  //TODO: show alert on MainWindow
    this->attachments.append(QPair<QString, QPair<QString, QString>>("doc",
                             QPair<QString,QString>(QString::number(ownerId < 0 ? -1 * ownerId : ownerId),
                                                    QString::number(docId))));
}

void TextEdit::RemoveAttachments()
{
    this->attachments.clear();
}

void TextEdit::SetLoadState()
{
    this->isLoading.lock();
}

void TextEdit::RemoveLoadState()
{
    this->isLoading.unlock();
}

