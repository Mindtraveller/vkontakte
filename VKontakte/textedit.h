#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include <QObject>
#include <QFrame>
#include <QtCore>
#include <QTextEdit>
#include <QKeyEvent>
#include <QList>
#include <QLabel>
#include <QLayout>
#include <QWidget>
#include <QPair>
#include <QString>
#include <QMutex>
#include "RequestManager.h"

class TextEdit : public QTextEdit
{
    Q_OBJECT
    QString recipient;
    QMutex isLoading;
    QList<int> forwardMessages;
    QList<QPair<QString, QPair<QString, QString>>> attachments;   //<type> <owner_id>_<media_id>
    void keyPressEvent(QKeyEvent *event);
    QString ForwardMessagesToSring();
    QString AttachmentToString();
public:
    TextEdit();
    TextEdit(QWidget *frame);

    void SetRecipient(QString userId);
    QString GetRecipient();

    void SetForwardMessages(QList<int> *messageIds);
    void RemoveForwardMessages();

    void AddPhotoToMessage(int photoId, int ownerId);
    void AddDocToMessage(int docId, int ownerId);

    void RemoveAttachments();

    void SetLoadState();
    void RemoveLoadState();

    ~TextEdit();
signals:
    void MessageSent();
};

#endif // TEXTEDIT_H
