#include "threadmanager.h"

ThreadManager *ThreadManager::instance = nullptr;

ThreadManager* ThreadManager::Init()
{
    if (instance == nullptr) instance = new ThreadManager();
    return instance;
}

void ThreadManager::StartThreads()
{
    //Here starts threads
    QThreadPool::start(longPollThread);
}

void ThreadManager::SetOnline()
{
    if (!this->onlineThread)
    {
        this->onlineThread = new SetOnlineThread();
        QThreadPool::start(this->onlineThread);
    }
}
void ThreadManager::SetOffline()
{
    if (this->onlineThread) this->onlineThread->Close();
}

ThreadManager::ThreadManager()
{
    this->longPollThread = new LongPollThread();
    this->onlineThread = NULL;
    QObject::connect(this->longPollThread,SIGNAL(Update(QJsonArray, int)),
                            this,SLOT(OnUpdate(QJsonArray, int)),Qt::DirectConnection);
}

void ThreadManager::OnUpdate(QJsonArray json, int type)
{
    switch(type)
    {
        case UpdateTypes::NewMessage:
            emit NewMessage(RequestManager::GetMessageById(QString::number(json[1].toInt())));
            break;
        case UpdateTypes::UsersActivity:
            emit UsersActivity(json);
            break;
        case UpdateTypes::Online:
        case UpdateTypes::Offline:
            RequestManager::RefreshCacheValue(-1*json[1].toInt());
            emit ChangedUsersState(-1*json[1].toInt());
            break;
        case UpdateTypes::MessageRead_2:
        case UpdateTypes::MessageRead:
            emit MessageRead(json[1].toInt());
            break;
        default:
            qDebug() << "____Event hasn't supported" << json;
            break;
    }
}

ThreadManager::~ThreadManager()
{
    this->longPollThread->Close();
    this->instance = nullptr;
}

void ThreadManager::Cache(QJsonArray json)
{
    QList<QString> list;
    foreach (auto value, json) {
        list.append(QString::number(value.toObject().value("uid").toInt()));
    }
    for(auto i =0; i < list.length()/5; i++)
    {
        auto cacheThread = new CacheThread(list.mid(i*5,5));
        QThreadPool::start(cacheThread);
    }
}

SleepThread* ThreadManager::GetSleepThread()
{
    return new SleepThread();
}

void ThreadManager::RunSleepThread(SleepThread* thread)
{
    QThreadPool::start(thread);
}


ThreadManager* ThreadManager::GetInstance()
{
    return instance;
}


