#ifndef THREADMANAGER_H
#define THREADMANAGER_H

#include <QThreadPool>
#include <QString>
#include <QObject>
#include <QJsonArray>
#include "longpollthread.h"
#include "cachethread.h"
#include "sleepthread.h"
#include "setonlinethread.h"


class ThreadManager : public QThreadPool
{
    Q_OBJECT

    LongPollThread* longPollThread;
    SetOnlineThread* onlineThread;
    static ThreadManager *instance;
    ThreadManager();

public:
    void StartThreads();
    void Cache(QJsonArray json);
    static ThreadManager* Init();
    static ThreadManager* GetInstance();
    static SleepThread* GetSleepThread();
    void RunSleepThread(SleepThread* thread);
    void SetOffline();
    void SetOnline();
    ~ThreadManager();
private slots:
    void OnUpdate(QJsonArray, int);
signals:
    void CacheDone();
    void MessageRead(int);
    void NewMessage(QJsonObject);
    void ChangedUsersState(int);
    void UsersActivity(QJsonArray);
};

#endif // THREADMANAGER_H
