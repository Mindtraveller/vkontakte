/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QWidget>
#include <textedit.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    QFrame *userInfoFrame;
    QLabel *lastNameLabel;
    QLabel *firstNameLabel;
    QLabel *photoLabel;
    QLabel *hometownLabel;
    QLabel *mobileLabel;
    QLabel *skypeLabel;
    QLabel *homePhoneLabel;
    QLabel *HometownLabelValue;
    QLabel *mobileLabelValue;
    QLabel *skypeLabelValue;
    QLabel *homePhoneLabelValue;
    QLabel *statusLabel;
    QCommandLinkButton *commandLinkButton;
    QListWidget *conversations;
    QFrame *dialogFrame;
    QListWidget *dialog;
    TextEdit *messageInput;
    QMenuBar *menuBar;
    QMenu *menuMain;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(782, 580);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(200, 0));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 255, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Light, brush1);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        QBrush brush2(QColor(120, 120, 120, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush2);
        QBrush brush3(QColor(160, 160, 160, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush4(QColor(247, 247, 247, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush4);
        QBrush brush5(QColor(255, 255, 220, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush5);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        QBrush brush6(QColor(240, 240, 240, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        MainWindow->setPalette(palette);
        QFont font;
        font.setPointSize(9);
        font.setBold(false);
        font.setWeight(50);
        font.setStrikeOut(false);
        font.setKerning(false);
        font.setStyleStrategy(QFont::PreferAntialias);
        MainWindow->setFont(font);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QStringLiteral("background:white;"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        userInfoFrame = new QFrame(centralWidget);
        userInfoFrame->setObjectName(QStringLiteral("userInfoFrame"));
        userInfoFrame->setGeometry(QRect(470, 270, 631, 301));
        QFont font1;
        font1.setBold(false);
        font1.setItalic(true);
        font1.setWeight(50);
        userInfoFrame->setFont(font1);
        userInfoFrame->setStyleSheet(QStringLiteral("background: #F0F5FF;"));
        userInfoFrame->setFrameShape(QFrame::Box);
        userInfoFrame->setFrameShadow(QFrame::Raised);
        lastNameLabel = new QLabel(userInfoFrame);
        lastNameLabel->setObjectName(QStringLiteral("lastNameLabel"));
        lastNameLabel->setGeometry(QRect(250, 20, 151, 20));
        QFont font2;
        font2.setPointSize(13);
        font2.setBold(false);
        font2.setItalic(false);
        font2.setUnderline(false);
        font2.setWeight(50);
        lastNameLabel->setFont(font2);
        lastNameLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        firstNameLabel = new QLabel(userInfoFrame);
        firstNameLabel->setObjectName(QStringLiteral("firstNameLabel"));
        firstNameLabel->setGeometry(QRect(410, 20, 161, 21));
        firstNameLabel->setFont(font2);
        firstNameLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        photoLabel = new QLabel(userInfoFrame);
        photoLabel->setObjectName(QStringLiteral("photoLabel"));
        photoLabel->setGeometry(QRect(10, 10, 221, 271));
        photoLabel->setStyleSheet(QLatin1String("padding: 5px;\n"
"background:#B8DBFF;"));
        photoLabel->setScaledContents(true);
        hometownLabel = new QLabel(userInfoFrame);
        hometownLabel->setObjectName(QStringLiteral("hometownLabel"));
        hometownLabel->setEnabled(true);
        hometownLabel->setGeometry(QRect(260, 110, 111, 16));
        QFont font3;
        font3.setPointSize(9);
        font3.setBold(false);
        font3.setItalic(false);
        font3.setWeight(50);
        font3.setKerning(true);
        font3.setStyleStrategy(QFont::PreferAntialias);
        hometownLabel->setFont(font3);
        hometownLabel->setMouseTracking(true);
        hometownLabel->setFrameShape(QFrame::NoFrame);
        hometownLabel->setFrameShadow(QFrame::Plain);
        hometownLabel->setLineWidth(1);
        hometownLabel->setTextFormat(Qt::AutoText);
        hometownLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        mobileLabel = new QLabel(userInfoFrame);
        mobileLabel->setObjectName(QStringLiteral("mobileLabel"));
        mobileLabel->setEnabled(true);
        mobileLabel->setGeometry(QRect(260, 140, 111, 16));
        mobileLabel->setFont(font3);
        mobileLabel->setMouseTracking(true);
        mobileLabel->setFrameShape(QFrame::NoFrame);
        mobileLabel->setFrameShadow(QFrame::Plain);
        mobileLabel->setLineWidth(1);
        mobileLabel->setTextFormat(Qt::AutoText);
        mobileLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        skypeLabel = new QLabel(userInfoFrame);
        skypeLabel->setObjectName(QStringLiteral("skypeLabel"));
        skypeLabel->setEnabled(true);
        skypeLabel->setGeometry(QRect(260, 160, 111, 16));
        skypeLabel->setFont(font3);
        skypeLabel->setMouseTracking(true);
        skypeLabel->setFrameShape(QFrame::NoFrame);
        skypeLabel->setFrameShadow(QFrame::Plain);
        skypeLabel->setLineWidth(1);
        skypeLabel->setTextFormat(Qt::AutoText);
        skypeLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        homePhoneLabel = new QLabel(userInfoFrame);
        homePhoneLabel->setObjectName(QStringLiteral("homePhoneLabel"));
        homePhoneLabel->setEnabled(true);
        homePhoneLabel->setGeometry(QRect(260, 180, 111, 16));
        homePhoneLabel->setFont(font3);
        homePhoneLabel->setMouseTracking(true);
        homePhoneLabel->setFrameShape(QFrame::NoFrame);
        homePhoneLabel->setFrameShadow(QFrame::Plain);
        homePhoneLabel->setLineWidth(1);
        homePhoneLabel->setTextFormat(Qt::AutoText);
        homePhoneLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        HometownLabelValue = new QLabel(userInfoFrame);
        HometownLabelValue->setObjectName(QStringLiteral("HometownLabelValue"));
        HometownLabelValue->setEnabled(true);
        HometownLabelValue->setGeometry(QRect(400, 110, 111, 16));
        HometownLabelValue->setFont(font3);
        HometownLabelValue->setMouseTracking(true);
        HometownLabelValue->setFrameShape(QFrame::NoFrame);
        HometownLabelValue->setFrameShadow(QFrame::Plain);
        HometownLabelValue->setLineWidth(1);
        HometownLabelValue->setTextFormat(Qt::AutoText);
        HometownLabelValue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        mobileLabelValue = new QLabel(userInfoFrame);
        mobileLabelValue->setObjectName(QStringLiteral("mobileLabelValue"));
        mobileLabelValue->setEnabled(true);
        mobileLabelValue->setGeometry(QRect(400, 140, 111, 16));
        mobileLabelValue->setFont(font3);
        mobileLabelValue->setMouseTracking(true);
        mobileLabelValue->setFrameShape(QFrame::NoFrame);
        mobileLabelValue->setFrameShadow(QFrame::Plain);
        mobileLabelValue->setLineWidth(1);
        mobileLabelValue->setTextFormat(Qt::AutoText);
        mobileLabelValue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        skypeLabelValue = new QLabel(userInfoFrame);
        skypeLabelValue->setObjectName(QStringLiteral("skypeLabelValue"));
        skypeLabelValue->setEnabled(true);
        skypeLabelValue->setGeometry(QRect(400, 160, 111, 16));
        skypeLabelValue->setFont(font3);
        skypeLabelValue->setMouseTracking(true);
        skypeLabelValue->setFrameShape(QFrame::NoFrame);
        skypeLabelValue->setFrameShadow(QFrame::Plain);
        skypeLabelValue->setLineWidth(1);
        skypeLabelValue->setTextFormat(Qt::AutoText);
        skypeLabelValue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        skypeLabelValue->setOpenExternalLinks(true);
        skypeLabelValue->setTextInteractionFlags(Qt::LinksAccessibleByKeyboard|Qt::LinksAccessibleByMouse|Qt::TextBrowserInteraction|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        homePhoneLabelValue = new QLabel(userInfoFrame);
        homePhoneLabelValue->setObjectName(QStringLiteral("homePhoneLabelValue"));
        homePhoneLabelValue->setEnabled(true);
        homePhoneLabelValue->setGeometry(QRect(400, 180, 111, 16));
        homePhoneLabelValue->setFont(font3);
        homePhoneLabelValue->setMouseTracking(true);
        homePhoneLabelValue->setFrameShape(QFrame::NoFrame);
        homePhoneLabelValue->setFrameShadow(QFrame::Plain);
        homePhoneLabelValue->setLineWidth(1);
        homePhoneLabelValue->setTextFormat(Qt::AutoText);
        homePhoneLabelValue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        statusLabel = new QLabel(userInfoFrame);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));
        statusLabel->setEnabled(true);
        statusLabel->setGeometry(QRect(260, 50, 351, 16));
        statusLabel->setFont(font3);
        statusLabel->setMouseTracking(true);
        statusLabel->setFrameShape(QFrame::NoFrame);
        statusLabel->setFrameShadow(QFrame::Plain);
        statusLabel->setLineWidth(1);
        statusLabel->setTextFormat(Qt::AutoText);
        statusLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        commandLinkButton = new QCommandLinkButton(centralWidget);
        commandLinkButton->setObjectName(QStringLiteral("commandLinkButton"));
        commandLinkButton->setGeometry(QRect(10, 0, 172, 41));
        conversations = new QListWidget(centralWidget);
        conversations->setObjectName(QStringLiteral("conversations"));
        conversations->setGeometry(QRect(0, 40, 231, 381));
        conversations->setAutoFillBackground(false);
        conversations->setProperty("showDropIndicator", QVariant(true));
        conversations->setResizeMode(QListView::Fixed);
        conversations->setWordWrap(false);
        dialogFrame = new QFrame(centralWidget);
        dialogFrame->setObjectName(QStringLiteral("dialogFrame"));
        dialogFrame->setGeometry(QRect(240, 40, 391, 391));
        dialogFrame->setFont(font1);
        dialogFrame->setStyleSheet(QStringLiteral("background: white;"));
        dialogFrame->setFrameShape(QFrame::Panel);
        dialogFrame->setFrameShadow(QFrame::Raised);
        dialog = new QListWidget(dialogFrame);
        dialog->setObjectName(QStringLiteral("dialog"));
        dialog->setGeometry(QRect(0, 0, 391, 321));
        dialog->setMouseTracking(false);
        dialog->setAutoFillBackground(false);
        dialog->setFrameShape(QFrame::NoFrame);
        dialog->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        dialog->setProperty("showDropIndicator", QVariant(true));
        dialog->setSelectionMode(QAbstractItemView::MultiSelection);
        dialog->setTextElideMode(Qt::ElideRight);
        dialog->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        dialog->setResizeMode(QListView::Fixed);
        dialog->setWordWrap(true);
        dialog->setSortingEnabled(false);
        messageInput = new TextEdit(dialogFrame);
        messageInput->setObjectName(QStringLiteral("messageInput"));
        messageInput->setGeometry(QRect(0, 320, 391, 71));
        messageInput->setAutoFillBackground(false);
        messageInput->setStyleSheet(QLatin1String("padding:10px;\n"
"border-top:1px solid lightblue;"));
        messageInput->setFrameShape(QFrame::NoFrame);
        messageInput->setFrameShadow(QFrame::Sunken);
        messageInput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        messageInput->setLineWrapMode(QTextEdit::WidgetWidth);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 782, 21));
        menuMain = new QMenu(menuBar);
        menuMain->setObjectName(QStringLiteral("menuMain"));
        menuMain->setSeparatorsCollapsible(false);
        menuMain->setToolTipsVisible(false);
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuMain->menuAction());
        menuMain->addSeparator();
        menuMain->addAction(actionExit);

        retranslateUi(MainWindow);

        dialog->setCurrentRow(-1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "VKontakte", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        lastNameLabel->setText(QString());
        firstNameLabel->setText(QString());
        photoLabel->setText(QString());
        hometownLabel->setText(QApplication::translate("MainWindow", "Hometown:", 0));
        mobileLabel->setText(QApplication::translate("MainWindow", "Mobile:", 0));
        skypeLabel->setText(QApplication::translate("MainWindow", "Skype:", 0));
        homePhoneLabel->setText(QApplication::translate("MainWindow", "Home phone:", 0));
        HometownLabelValue->setText(QString());
        mobileLabelValue->setText(QString());
        skypeLabelValue->setText(QString());
        homePhoneLabelValue->setText(QString());
        statusLabel->setText(QString());
        commandLinkButton->setText(QApplication::translate("MainWindow", "Messages", 0));
        messageInput->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", 0));
        menuMain->setTitle(QApplication::translate("MainWindow", "Main", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
